# see https://adventofcode.com/2021/day/22

require_relative "./cube"

class Rebooter
  attr_accessor :nb_dimension, :cubes

  # extra parameter (cubes) is only for debuging/testing
  def initialize(nb_dimension, cubes = [])
    self.nb_dimension = nb_dimension
    self.cubes = cubes # a list of non intersecting/overlaping cubes of nb_dimension
  end

  def process(steps)
    steps.each { |step| process_step!(*step) }
    size
  end

  # returns the number of switch on
  def size
    cubes.collect(&:size).sum
  end

  # update and returns the new list of cube
  def process_step!(is_on, *ranges)
    new_cube = Cube.new(*ranges)
    self.cubes = cubes.inject(is_on ? [new_cube] : []) do |l, cube|
      l.concat(cube.remove(new_cube))
    end
  end
end
