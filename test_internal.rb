require "./rebooter"
require "./cube"
require "test/unit/assertions"

include Test::Unit::Assertions

# range intersection
assert_nil (1..3).compute_intersection(5..7)
assert_equal 5..5, (1..5).compute_intersection(5..7)
assert_equal 5..7, (1..10).compute_intersection(5..7)
assert_equal 5..6, (1..6).compute_intersection(5..7)

# testing enumeration
assert_equal [[:a, 1], [:a, 2], [:a, 3], [:b, 1], [:b, 2], [:b, 3]], [[:a, :b], [1, 2, 3]].expanser

#  testing in 1 dimension
rebooter = Rebooter.new(1, [Cube.new(1..3), Cube.new(5..7)])
rebooter.process_step!(true, 100..110)
assert_equal [Cube.new(100..110), Cube.new(1..3), Cube.new(5..7)], rebooter.cubes

rebooter.process_step!(true, -10..-4)
rebooter.process_step!(true, 15..25)
assert_equal [Cube.new(15..25), Cube.new(-10..-4), Cube.new(100..110), Cube.new(1..3), Cube.new(5..7)], rebooter.cubes

rebooter.process_step!(true, -6..6)
assert_equal [Cube.new(-6..6), Cube.new(15..25), Cube.new(-10..-7), Cube.new(100..110), Cube.new(7..7)], rebooter.cubes
rebooter.process_step!(true, 22..100)
assert_equal [Cube.new(22..100), Cube.new(-6..6), Cube.new(15..21), Cube.new(-10..-7), Cube.new(101..110), Cube.new(7..7)], rebooter.cubes

rebooter.process_step!(false, 2..19)
assert_equal [Cube.new(22..100), Cube.new(-6..1), Cube.new(20..21), Cube.new(-10..-7), Cube.new(101..110)], rebooter.cubes

rebooter.process_step!(false, 1000..2000)
assert_equal [Cube.new(22..100), Cube.new(-6..1), Cube.new(20..21), Cube.new(-10..-7), Cube.new(101..110)], rebooter.cubes

rebooter.process_step!(false, -1000..2000)
assert_equal [], rebooter.cubes

# testing in 2 dimensions
rebooter = Rebooter.new(2)
rebooter.process_step!(true, 1..10, 1..10)
assert_equal 100, rebooter.size
assert_equal [Cube.new(1..10, 1..10)], rebooter.cubes
rebooter.process_step!(false, 1..1, 1..1)
assert_equal 99, rebooter.size
