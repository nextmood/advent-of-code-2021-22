# A pure ruby solution to [Advent of Code 2021/22](https://adventofcode.com/2021/day/22)

## Requirements
Tested with ruby 3.0.3 (should work with 2.x)

## Usage
```
# see ./test.rb
ruby test.rb
```
## Note
* the problem to solve involves a cube of 3 dimensions (x,y,z), this solution works for any number of dimension.
* use [Ruby Standard Style](https://github.com/testdouble/standard)
