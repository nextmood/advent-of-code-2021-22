require "test/unit/assertions"

# Cube defines a set of point in a N dimensional discrete space
# A Cube is a :
# line segment in dimension 1
# rectangle in dimension 2
# cube in dimension 3
# hyper-cube in dimension > 3
#
# A Cube is defined by an ordered list of Range (1 Range per dimension)
class Cube
  include Test::Unit::Assertions

  attr_accessor :ranges

  def initialize(*ranges)
    self.ranges = ranges
  end

  # compute the number of point in the volume
  def size
    ranges.collect(&:size).inject(:*)
  end

  def nb_dimension
    @nb_dimension ||= ranges.size
  end

  def ==(other)
    other.ranges == ranges
  end

  def to_s
    nb_dimension.times.collect { |di| "d#{di}=#{ranges[di]}" }.join(", ")
  end

  # remove an other Cube intersection from this Cube (self)
  # always returns a list of Cube
  # if the cubes dont intersect, then return: [self]
  # otherwise returns the list of (sub)Cube included into self that doesn't intersect other 
  # we have: self == intersection(other, self) + list_of_subCube
  def remove(other)
    self - compute_intersection(other)
  end

  # return the intersection (a Cube) between this Cube (self) and an other Cube
  # return nil if no intersection
  def compute_intersection(other)
    # we compute the intersection for each dimension
    intersection_ranges = ranges.collect.with_index do |range, di|
      other.ranges[di].compute_intersection(ranges[di])
    end
    # an intersection exists when there is an intersection in each dimension
    Cube.new(*intersection_ranges) if intersection_ranges.all?
  end

  # returns a list of complementary Cube
  def -(other)
    return [self] unless other
    # this is the smert part !
    complementary_ranges = nb_dimension.times.collect do |di|
      [
        Range.new_safe(ranges[di].min, other.ranges[di].min - 1),
        Range.new_safe(other.ranges[di].min, other.ranges[di].max),
        Range.new_safe(other.ranges[di].max + 1, ranges[di].max)
      ].compact
    end.expanser
    complementary_ranges.delete(other.ranges)
    assert(complementary_ranges.size <= 3**nb_dimension - 1)
    complementary_ranges.collect { |ranges| Cube.new(*ranges) }
  end
end

class Range
  # return the intersection (a Range) between this Range (self) and an other Range
  # return nil if no intersection
  def compute_intersection(other)
    unless max < other.min || min > other.max
      [min, other.min].max..[max, other.max].min
    end
  end

  def self.new_safe(min, max)
    min..max if min <= max
  end
end

class Array
  # expecting self to be a list of list, for example [[rx1, rx2, rx3], [ry1, ry2]]
  # returns a list of Tupple => [[rx1, ry1], [rx1, ry2], [rx2, ry1], [rx2, ry2], [rx3, ry1], [rx3, ry2]]
  # where tupple.size == lists.size
  def expanser(heap = [])
    if empty?
      [heap]
    else
      list, *tail = self
      list.collect_concat { |v| tail.expanser([*heap, v]) }
    end
  end
end
